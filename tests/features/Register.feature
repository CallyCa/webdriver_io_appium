Feature: Register validations

        Background:
            Given I'm on the Register Screen
    
        Scenario Outline: I'm will register a user account
             When I click on button New register
             Then Will show the button Next
             When I click on button Next, will show two buttons
             Then I choose the button I'm a User
              And I put on input name: '<Name>'
              And Input on input sirname: '<Sirname>'
             Then I click on button confirm
             When I put the E-mail: '<Email>'
             Then I click on button confirm
              And I click on button to close screen

        Examples:
                  | Email                  | Name | Sirname |
                  | ciyoto3790@1uscare.com | Tesz | abacate |
            
        Scenario Outline: I'm will register a user account and i'll fill as space in white on input sirname
             When I click on button New register
             Then Will show the button Next
             When I click on button Next, will show two buttons
             Then I choose the button I'm a User
              And I put on input name: '<Name>'
              And input on input sirname: '<Sirname>'
             Then I click on button confirm
             When I put the E-mail: '<Email>'
             Then I click on button confirm
              And I click on button to close screen

        Examples:
                  | Email                  | Name | Sirname |
                  | ciyoto3790@1uscare.com | Tesz |         |
    
        Scenario Outline: I'm will register a company account
             When I click on button New register
             Then Will show the button Next
             When I click on button Next, will show two buttons
             Then I choose the button I'm a Company
              And I put on input name: '<Name>'
             Then I click on button confirm
             When I put the E-mail: '<Email>'
             Then I click on button confirm
              And I click on button to close screen

        Examples:
                  | Email               | Name   |
                  | gonon21730@awg5.com | E-corp |