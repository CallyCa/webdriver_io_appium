Feature: Profile validations

        Background:
            Given I'm on the Profile screen

        Scenario Outline: Open Information account and click in see my profile and edit profile
             When I open the Information account
             Then I click on button see my profile
              And click in Edit profile
              And I fill fields name: '<Name>', sirname: '<Sirname>'
              And I fill field bio: '<Bio>' and I click on button confirm
              And I select my biologic sex: '<Sex>'
              And I select my civil state: '<CivilState>'
              And I click in all buttons of Relation with Autism
             Then I click in save button
              And return to the profile
              
        Examples:
                  | Name   | Sirname | Bio            | Sex | CivilState |
                  | Thiago | Cally   | Notas de bloco | M   | Solteiro   |

        Scenario: Open Information account, click in see my profile, enter in the followers and block user
             When I open the Information account
             Then I click on button see my profile
             When I click on Follower
             Then I click in menu option
              And I click block user
              And confirm block user

        Scenario: Open Information account, click in see my profile, enter in the followers and report user
             When I open the Information account
             Then I click on button see my profile
             When I click on Follower
             Then I click in menu option
              And I click report User
              And choose first option
              And return to the profile

        Scenario: Open Information account, click in see my profile, create a publication and like publi
             When I open the Information account
             Then I click on button see my profile
             When I click on button create publication
             Then I write 'Isso eh uma publicacao legal'
              And Click in the button publication
              And like the publication

        Scenario: Open Information account, click in see my profile and look at all photos
             When I open the Information account
             Then I click on button see my profile
             When I click on button see all photos
             Then check if the photos are visible
              And click in the first photo
              And return to the profile

        Scenario: Open Information account, click in see my profile, look at all photos and click in the last photo on profile
             When I open the Information account
             Then I click on button see my profile
             When I click on button see all photos
             Then check if the photos are visible
              And click in the first photo
              And return to the profile
              And click in the last photo
            
        Scenario: Open Information account, click in see my profile, look photo in the profile
             When I open the Information account
             Then I click on button see my profile
             When I click in the photo on profile
             Then return to the profile
