Feature: Forgot my password validations

        Background:
            Given I'm on the Forgot my password Screen
    
        Scenario Outline: I'm will register a user account
             When I click on button Forgot my password
             Then I put the E-mail: '<Email>'
              And I click on button Send
             Then I put the code what will be send on email, code: '<Code>'
              And I click on button send Token
             When I send Token, verify if two inputs to be visible
             Then put new password: '<NewPassword>' and confirm new password: '<ConfirmNewPassword>'
              And I click on button confirm
             Then close screen

        Examples:
                  | Email                  | Code | NewPassword  | ConfirmNewPassword |
                  | ciyoto3790@1uscare.com | 6184 | teste@123456 | teste@123456       |
        