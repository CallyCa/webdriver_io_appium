Feature: Informations User validations

        Background:
            Given I'm on the Information Account screen

        Scenario: Open invite and copy shared link
             When I close the modal update app
             Then I click in Invite
             When I click in shared link
             Then the app show which apps can be shared with link
             When I click in copy
             Then return to the menu information

        Scenario: Open invite and click much times on input
             When I enter in the Invite menu again
              And I click three times on input
             Then the app show two buttons
              And I click in select all
              And I'll click in copy url
              And I click in shared link button
             When I click in copy link
             Then I will return to the information menu

        Scenario Outline: Open information account and select Alter Email
          #    When I click in the menu information account
             Then I click in change email
             When I try to credentials; password: '<Password>', email: '<Email>' and confirm email: '<Email>'
             Then return to the information account

        Examples:
                  | Email                | Password     |
                  | user@123456          | 12346.346    |
                  | 3243g@user           | /12346569    |
                  | yaltedDfe@userax.com | xademeoz54!# |

        Scenario Outline: Open information account and I as user would select Alter Password
          #    When I click on the menu information account
             Then I click in change password
             When I try to credentials; password: '<Password>', password: '<Password>' and confirm password: '<Password>'
             Then return to the menu information account

        Examples:
                  | Password     |
                  | user@123456  |
                  | 12346.346    |
                  | 3243g@user   |
                  | /12346569    |
                  | xademeoz54!# |
                  | neodemon.s$# |

        Scenario Outline: Open Biometric Access and click button cancel
          #    When I click in the menu information account
              And I click in Biometric Access
             Then I click on button Biometric Access
             When put the account password: '<Password>'
              And I click on cancel button
             Then i'll return to the information account

        Examples:
                  | Password     |
                  | 12346.346    |
                  | /12346569    |
                  | xademeoz54!# |

        Scenario Outline: Open Biometric Access and click button confirm
          #    When I click in the menu information account
              And I click in Biometric Access
             Then put the account password: '<Password>'
             When I click on button confirm
              And the user will be returning to the information account

        Examples:
                  | Password     |
                  | 12346.346    |
                  | /12346569    |
                  | xademeoz54!# |

        Scenario: Open blocked users
          #    When I click in the menu information account
             When I click in blocked users
              And I click unblock
             Then I click unblock user
              And return to Information User screen

        Scenario: Open Menu Configuration
             When I open Menu Configuration
              And I click in Dark mode
             Then I click Accessibility menu
              And I click in emoji
              And I click in emoji describe
             When I click on button Dark Mode again, but now will be three times
             Then I'll return to Information User screen

        Scenario: Open Menu About Tismoo
             When I click in Menu About Tismoo
             Then return to Information User screen

        Scenario: Open Menu Privacity
             When I click in Terms and Privacity
             Then return to the Information User screen

        Scenario: Click on button Exit account
             When I click on button Exit
             Then I click in the option Cancel

        Scenario: Exit account
             When I click in Exit
             Then I will click in the option Confirm and exit account

