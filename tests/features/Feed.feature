Feature: Feed validations

        Background:
            Given I'm on the feed screen

        Scenario: Create a publication
             When I click on the button create a publication
            #  Then I write the text 'It not a text' on the text field
             When I click on the button send publication
             Then return to the feed

        Scenario: Click the like button
             When the like button stays visible to the user
             Then I click on the like button

        Scenario: Click the like button and click favorite publication
             When the button like and favorite button stay visible to the user
             Then I click on both buttons at equal time
             When I click the favorite button
             Then I will return to the feed
             When clicking in the comment
          #    Then I write 'I wrote this yesterday, bro' in a publication
          #     And click in send publication
             Then I'm returning to the feed

        Scenario: Open the menu and select the options copy text or delete publication
             When I click on the menu
             Then the menu will show the options copy text or delete publication
             When I click in the Copy text
             Then the menu will be closed and I click again on the menu
             When I click Delete publication
             Then the app will show us if we want to cancel or delete
              And click on an option cancel
             When return to feed verify if the button menu is active
             Then open the menu
              And I click delete publication
             When I click on an option confirm
             Then the publication will be deleted

     #    Scenario: Open the menu and select the option report publication
     #         When I click on the menu again
     #         Then the menu will show the options copy text and report publication
     #         When I click in the report publication
     #          And I click on first option
     #         Then will show a modal and I'll click OK
     #           And I'll copy publication again

