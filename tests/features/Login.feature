Feature: Login validations

        Background:
            Given I'm on the login screen

        Scenario Outline: Displays warning message when logging in with invalid credentials
             When I try to log in with credentials; email: '<Email>' and password: '<Password>'
             Then app displays a message that the credentials are invalid
              And app show a message asking if want enable Biometric Access
             Then I click No
             
        Examples:
                  | Email                | Password    |
                  | user@123456          | 12346.346   |
                  | 3243g@user           | /12346569   |
                  | yosemite@dotecks.com | teste123    |
                  | specaftg@typefcz.com | Junyesz@d1! |

        Scenario Outline: Displays warning message when logging in with valid credentials
             When I try to log in with credentials; email: '<Email>' and password: '<Password>'
             Then app displays alert of successful login
              And app show a message asking if want enable Biometric Access
             Then I click Yes

        
        Examples:
                  | Email                | Password |
                  | yosemite@dotecks.com | teste123 |
                  | specaftg@typefcz.com | teste132 |
