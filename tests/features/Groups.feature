Feature: Groups validations

        Background:
            Given I'm on the Groups screen

        Scenario Outline: Open Groups and create new group
             When I open the Groups
             Then I click on button New Group
              And fill input fields name: '<Name>', description: '<Description>'
             Then I click on button create

        Examples:
                  | Name        | Description |
                  | teste_grupo | a group     |
    
        Scenario Outline: Enter on the my group
             When I click on button Administrator
             Then Enter in my group
             When I click on button Edit
             Then I'll alter the name to: '<Name>' and description to: '<Description>'
              And I click on button save
             Then return to the groups

        Examples:
                  | Name         | Description             |
                  | alterar_nome | aleatoriamente um grupo |
        
        Scenario Outline: Enter on the my group and create a new publication
             When I click on button Administrator
             Then Enter in my group
             When I click on button create a publication
             Then I write a publication: '<Publication>'
              And I click on button send
             Then return to the groups
             
        Examples:
                  | Publication         |
                  | publicacao de teste |
 