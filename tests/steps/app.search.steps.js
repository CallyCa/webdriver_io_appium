import { Given, When, Then } from '@cucumber/cucumber';
// import { expect } from 'chai';
import { searchSelect } from '../screenobjects/components/SearchSelector';
import { feedSelect } from '../screenobjects/components/FeedSelectors';
// import SearchScreen from '../screenobjects/search.screen';

Given(/^I'm on the Search Users screen$/, () => {
    searchSelect.MenuSearchButton();
    feedSelect.WaitUpdateAppNo();
    feedSelect.UpdateAppNo();
});

When(
    /^Enter on the Search Users, verify whether username is displayed$/,
    () => {
        searchSelect.WaitClickUserProfile();
        searchSelect.ShowsClickUserProfile();
    },
);

Then(/^I click in the first user$/, () => {
    searchSelect.ClickUserProfile();
});

When(/^I'll click in follow$/, () => {
    searchSelect.WaitFollowUser();
    searchSelect.FollowUser();
});

When(/^I click in menu option$/, () => {
    searchSelect.MenuOptionsProfileUser();
});

When(/^I click on option block User$/, () => {
    searchSelect.MenuBlockUser();
});

Then(/^I block user$/, () => {
    searchSelect.BlockUser();
});

When(/^confirm block user$/, () => {
    expect(searchSelect.ConfirmBlockUser().isDisplayed()).to.be.true;
    searchSelect.ConfirmBlockUser();
});

Then(/^I will unblock user$/, () => {
    searchSelect.UnblockUser();
});

When(/^I'll confirm unblock user$/, () => {
    searchSelect.ConfirmUnblockUser();
});

When(/^I click on button OK$/, () => {
    searchSelect.ConfirmBlockUser();
});

Then(/^I'll click in follow again$/, () => {
    searchSelect.FollowUser();
});

When(/^'ll return to the Search users$/, () => {
    searchSelect.returnButton();
});
