import { Given, When, Then } from '@cucumber/cucumber';
import { expect } from 'chai';
import { registerSelect } from '../screenobjects/components/RegisterSelector';
import RegisterScreen from '../screenobjects/Register.screen';

Given(/^I'm on the Register Screen$/, () => {
    registerSelect.WaitCreateUsersButton(true);
});

When(/^I click on button New register$/, () => {
    registerSelect.CreateUsersButton();
});

Then(/^Will show the button Next$/, () => {
    expect(registerSelect.ClickButtonNext().isDisplayed()).to.be.true;
});

When(/^I click on button Next, will show two buttons$/, () => {
    registerSelect.ClickButtonNext();
    expect(registerSelect.CreateUserCommom().isDisplayed()).to.be.true;
    expect(registerSelect.CreateUserCompany().isDisplayed()).to.be.true;
});

Then(/^I choose the button I'm a User$/, () => {
    registerSelect.CreateUserCommom();
});

When(/^I put on input name: '(.+)'$/, (text) => {
    RegisterScreen.RegisterNameUser.setValue(text);
});

When(/^Input on input sirname: '(.+)'$/, (text) => {
    RegisterScreen.RegisterSirNameUser.setValue(text);
});

Then(/^I click on button confirm$/, () => {
    registerSelect.ClickButtonOK();
});

When(/^I put the E-mail: '(.+)'$/, (text) => {
    RegisterScreen.Register_Email.setValue(text);
});

Then(/^I click on button confirm$/, () => {
    registerSelect.ClickButtonOK();
});

When(/^I click on button to close screen$/, () => {
    registerSelect.returnButton();
});

When(/^I click on button New register$/, () => {
    registerSelect.CreateUsersButton();
});

Then(/^Will show the button Next$/, () => {
    expect(registerSelect.ClickButtonNext().isDisplayed()).to.be.true;
});

When(/^I click on button Next, will show two buttons$/, () => {
    registerSelect.ClickButtonNext();
    expect(registerSelect.CreateUserCommom().isDisplayed()).to.be.true;
    expect(registerSelect.CreateUserCompany().isDisplayed()).to.be.true;
});

Then(/^I choose the button I'm a User$/, () => {
    registerSelect.CreateUserCommom();
});

When(/^I put on input name: '(.+)'$/, (text) => {
    RegisterScreen.RegisterNameUser.setValue(text);
});

When(/^I put on input sirname: '(.+)'$/, (text) => {
    RegisterScreen.RegisterSirNameUser.setValue(text);
});

Then(/^I click on button confirm$/, () => {
    registerSelect.ClickButtonOK();
});

When(/^I put the E-mail: '(.+)'$/, (text) => {
    RegisterScreen.Register_Email.setValue(text);
});

Then(/^I click on button confirm$/, () => {
    registerSelect.ClickButtonOK();
});

When(/^I click on button to close screen$/, () => {
    registerSelect.returnButton();
});

When(/^I click on button New register$/, () => {
    registerSelect.CreateUserCommom();
});

Then(/^Will show the button Next$/, () => {
    expect(registerSelect.ClickButtonNext().isDisplayed()).to.be.true;
});

When(/^I click on button Next, will show two buttons$/, () => {
    registerSelect.ClickButtonNext();
    expect(registerSelect.CreateUserCommom().isDisplayed()).to.be.true;
    expect(registerSelect.CreateUserCompany().isDisplayed()).to.be.true;
});

Then(/^I choose the button I'm a Company$/, () => {
    registerSelect.CreateUserCompany();
});

When(/^I put on input name: '(.+)'$/, (text) => {
    registerSelect.RegisterCompanyName.setValue(text);
});

Then(/^I click on button confirm$/, () => {
    registerSelect.ClickButtonOK();
});

When(/^I put the E-mail: '(.+)'$/, (text) => {
    RegisterScreen.Register_Email.setValue(text);
});

Then(/^I click on button confirm$/, () => {
    registerSelect.ClickButtonOK();
});

When(/^I click on button to close screen$/, () => {
    registerSelect.returnButton();
});
