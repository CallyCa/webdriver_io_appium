// import { assert } from 'chai';
import { Given, When, Then } from '@cucumber/cucumber';
import LoginScreen from '../screenobjects/login.screen';
import { loginSelect } from '../screenobjects/components/LoginSelector';

Given(/^I'm on the login screen$/, () => {
    loginSelect.returnButton();
    loginSelect.waitForInputEmailShown(true);
    LoginScreen.waitForInputPasswdShown(true);
});

When(
    /^I try to log in with credentials; email: '(.+)' and password: '(.+)'$/,
    (email, password) => {
        LoginScreen.login(email, password);
    },
);

Then(/^app displays a message that the credentials are invalid$/, () => {
    LoginScreen.errorMessageLogin(true);
    LoginScreen.errorMessageLoginClick();
});

When(/^app show a message asking if want enable Biometric Access'$/, () => {
    LoginScreen.alertBiometric();
});

Then(/^I click No'$/, () => {
    LoginScreen.alertBiometricMessageNO();
});

When(
    /^I try to log in with credentials; email: '(.+)' and password: '(.+)'$/,
    (email, password) => {
        LoginScreen.login(email, password);
    },
);

Then(/^app displays alert of successful login$/, () => {
    LoginScreen.waitForBiometricShown(true);
});

When(/^app show a message asking if want enable Biometric Access$/, () => {
    LoginScreen.waitForBiometricShown(true);
});

Then(/^I click Yes$/, () => {
    LoginScreen.alertBiometricMessageYes();
});
