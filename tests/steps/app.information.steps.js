import { Given, When, Then } from '@cucumber/cucumber';
// import { expect } from 'chai';
import SelectorsInformationScreen from '../screenobjects/InformationAccountSelectors.screen';
import { informationSelect } from '../screenobjects/components/InformationsAccountSelectors';
import { feedSelect } from '../screenobjects/components/FeedSelectors';

Given(/^I'm on the Information Account screen$/, () => {
    informationSelect.SwitchedButtonInformation();
});

When(/^I close the modal update app$/, () => {
    feedSelect.WaitUpdateAppNo();
    feedSelect.UpdateAppNo();
});

Then(/^I click in Invite$/, () => {
    informationSelect.InviteButton();
});

When(/^I click in shared link$/, () => {
    informationSelect.WaitForInvite(true);
    informationSelect.InviteLinkButton();
});

Then(/^the app show which apps can be shared with link$/, () => {
    informationSelect.VerifyClickCopyLinkButton();
});

When(/^I click in copy$/, () => {
    informationSelect.ClickCopyLinkButton();
});

Then(/^return to the menu information$/, () => {
    informationSelect.ButtonClose();
});

When(/^I enter in the Invite menu again$/, () => {
    informationSelect.InviteButton();
});

When(/^I click three times on input$/, () => {
    informationSelect.ClickInputInvite();
});

Then(/^the app show two buttons$/, () => {
    informationSelect.VerifyInviteSelectAll();
    informationSelect.VerifyInviteCopyAll();
});

When(/^I click in select all$/, () => {
    informationSelect.InputInviteSelectAll();
});

When(/^I'll click in copy url$/, () => {
    informationSelect.InputInviteCopyAll();
});

When(/^I click in shared link button$/, () => {
    informationSelect.InviteLinkButton();
});

When(/^I click in copy link$/, () => {
    informationSelect.ClickCopyLinkButton();
});

Then(/^I will return to the information menu$/, () => {
    informationSelect.ButtonClose();
});

When(/^I click in the menu information account$/, () => {
    informationSelect.InformationsButton();
});

Then(/^I click in change email$/, () => {
    informationSelect.AlterEmailButton();
});

When(
    /^I try to credentials; password: '(.+)', email: '(.+)' and confirm email: '(.+)'$/,
    (password, email, confirmEmail) => {
        SelectorsInformationScreen.ChangeEmail(password, email, confirmEmail);
    },
);

Then(/^return to the information account$/, () => {
    informationSelect.ButtonClose();
});

When(/^I click on the menu information account$/, () => {
    informationSelect.InformationsButton();
});

Then(/^I click in change password$/, () => {
    informationSelect.AlterPassword();
});

When(
    /^I try to credentials; password: '(.+)', password: '(.+)' and confirm password: '(.+)'$/,
    (passwd, password, confirmPassword) => {
        SelectorsInformationScreen.ChangePassword(
            passwd,
            password,
            confirmPassword,
        );
    },
);

Then(/^return to the menu information account$/, () => {
    informationSelect.ButtonClose();
});

// STEP 1

When(/^I click in the menu information account$/, () => {
    informationSelect.InformationsButton();
});

When(/^I click in Biometric Access$/, () => {
    informationSelect.Fingerprint();
});

Then(/^I click on button Biometric Access$/, () => {
    informationSelect.Fingerprint();
});

When(/^put the account password: '(.+)'$/, () => {
    informationSelect.PutPasswordBiometric();
});

When(/^I click on cancel button$/, () => {
    informationSelect.FingerprintCancelButton();
});

Then(/^i'll return to the information account$/, () => {
    informationSelect.ButtonClose();
});

// STEP 2

When(/^I click in the menu information account$/, () => {
    informationSelect.InformationsButton();
});

When(/^I click on button Biometric Access$/, () => {
    informationSelect.Fingerprint();
});

When(/^put the account password: '(.+)'$/, () => {
    informationSelect.PutPasswordBiometric();
});

When(/^I click on button confirm'$/, () => {
    informationSelect.FingerprintConfirmButton();
});

When(/^the user will be returning to the information account$/, () => {
    informationSelect.ButtonClose();
});

When(/^I click in the menu information account$/, () => {
    informationSelect.InformationsButton();
});

When(/^I click in blocked users$/, () => {
    informationSelect.BlockedUsers();
});

When(/^I click unblock$/, () => {
    informationSelect.UnblockUsers();
});

Then(/^I click unblock user$/, () => {
    informationSelect.UnblockUsersConfirm();
});

When(/^return to Information User screen$/, () => {
    informationSelect.ButtonClose();
    informationSelect.ButtonClose();
});

When(/^I open Menu Configuration$/, () => {
    informationSelect.ConfigurationButton();
});

When(/^I click in Dark mode$/, () => {
    informationSelect.DarkMode();
});

When(/^I click Accessibility menu$/, () => {
    informationSelect.ConfigurationsAcessibility();
});

When(/^I click in emoji$/, () => {
    informationSelect.AcessibilityViewEmoji();
});

When(/^I click in emoji describe$/, () => {
    informationSelect.AcessibilityDescription();
    informationSelect.ButtonClose();
});

When(/^I click on button Dark Mode again, but now will be three times$/, () => {
    informationSelect.ForDarkMode();
});

Then(/^I'll return to Information User screen$/, () => {
    informationSelect.ButtonClose();
});

When(/^I click in Menu About Tismoo$/, () => {
    informationSelect.AboutUsButton();
});

Then(/^return to Information User screen$/, () => {
    informationSelect.ButtonClose();
});

When(/^I click in Terms and Privacity$/, () => {
    informationSelect.PrivacityButton();
});

Then(/^return to the Information User screen$/, () => {
    informationSelect.ButtonClose();
});

When(/^I click on button Exit$/, () => {
    informationSelect.ExitButton();
});

Then(/^I click in the option Cancel$/, () => {
    informationSelect.ExitCancelButton();
});

When(/^I click in Exit$/, () => {
    informationSelect.ExitButton();
});

Then(/^I will click in the option Confirm and exit account$/, () => {
    informationSelect.ExitConfirmButton();
});
