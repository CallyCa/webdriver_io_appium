import { Given, When, Then } from '@cucumber/cucumber';
// import { assert } from 'chai';
// import SelectorsFeedScreen from '../screenobjects/FeedSelectors.screen';
import { feedSelect } from '../screenobjects/components/FeedSelectors';

Given(/^I'm on the feed screen$/, () => {
    feedSelect.waitForTabBarShown(true);
});

When(/^I click on the button create a publication$/, () => {
    feedSelect.UpdateAppNo();
    feedSelect.openForms();
});

// Then(/^I write the text ('.+') on the text field/, (text) => {
//     SelectorsFeedScreen.createPosts.setValue(text)

//     /**
//      * IMPORTANT!!
//      *  Because the app is not closed and opened between the tests
//      *  (and thus is NOT starting with the keyboard hidden)
//      *  the keyboard is closed here if it is still visible.
//      */
//     if (driver.isKeyboardShown()) {
//         driver.hideKeyboard();
//     }
// });

When(/^I click on the button send publication$/, () => {
    feedSelect.sendPublication();
});

Then(/^return to the feed$/, () => {
    feedSelect.returnButton();
});

When(/^the like button stays visible to the user$/, () => {
    feedSelect.waitButtonLike(true);
});

Then(/^I click on the like button$/, () => {
    feedSelect.buttonLike();
});

When(/^the button like and favorite button stay visible to the user$/, () => {
    feedSelect.waitButtonLike(true);
    feedSelect.waitFavoritedButton(true);
});

Then(/^I click on both buttons at equal time$/, () => {
    feedSelect.favoritedButton();
    feedSelect.buttonLike();
});

When(/^I click the favorite button$/, () => {
    feedSelect.favoriteIntern();
});

Then(/^I will return to the feed$/, () => {
    feedSelect.returnFavoriteButton();
});

When(/^clicking in the comment$/, () => {
    feedSelect.commentButton();
});

// Then(/^I write '(.+)' in a publication$/, (text) => {
//     SelectorsFeedScreen.inputComment.setValue(text);
// });

// When(/^click in send publication$/, () => {
//     feedSelect.waitButtonLike(true);
//     feedSelect.sendComment();
// assert.equal(
//     SelectorsFeedScreen.sendComment.getText(),
//     'I wrote this yesterday, bro',
// );
// });

Then(/^I'm returning to the feed$/, () => {
    feedSelect.returnButton();
});

When(/^I click on the menu$/, () => {
    feedSelect.menuOptions();
});

Then(/^the menu will show the options copy text or delete publication$/, () => {
    feedSelect.VerifyMenuOptionCopy();

    // assert.equal(feedSelect.VerifymenuOptionDelete, 'Excluir publicação');
});

When(/^I click in the Copy text$/, () => {
    feedSelect.menuOptionCopy();
});

Then(/^the menu will be closed and I click again on the menu$/, () => {
    feedSelect.menuOptions();
});

When(/^I click Delete publication$/, () => {
    feedSelect.menuOptionDelete();
});

Then(/^the app will show us if we want to cancel or delete$/, () => {
    feedSelect.VerifyMenuOptionDeleteConfirm();

    feedSelect.VerifyMenuOptionDeleteCancel();
});

When(/^click on an option cancel$/, () => {
    feedSelect.menuOptionDeleteCancel();
});

When(/^return to feed verify if the button menu is active$/, () => {
    feedSelect.waitForTabBarShownMenu(true);
});

Then(/^open the menu$/, () => {
    feedSelect.menuOptions();
});

When(/^I click delete publication$/, () => {
    feedSelect.menuOptionDelete();
});

When(/^I click on an option confirm$/, () => {
    feedSelect.menuOptionDeleteConfirm();
});

Then(/^the publication will be deleted$/, () => {
    feedSelect.waitButtonLike();
    feedSelect.VerifyButtonLike();
});

// When(/^I click on the menu again$/, () => {
//     feedSelect.menuOptions();
// });

// Then(
//     /^the menu will show the options copy text and report publication$/,
//     () => {
//         feedSelect.VerifyMenuOptionCopy();
//         feedSelect.VerifyMenuOptionReport();
//     },
// );

// When(/^I click in the report publication$/, () => {
//     feedSelect.menuOptionReport();
// });

// When(/^I click on first option$/, () => {
//     feedSelect.firstOptionReport();
// });

// Then(/^will show a modal and I'll click OK$/, () => {
//     feedSelect.VerifyModalMessageOk();
//     feedSelect.modalMessageOk();
// });

// When(/^I'll copy publication again$/, () => {
//     feedSelect.VerifyMenuOptionCopy();
//     feedSelect.menuOptionCopy();
// });
