import { Given, When, Then } from '@cucumber/cucumber';
// import { assert, expect } from 'chai';
// import SelectorsFeedScreen from '../screenobjects/Groups.screen';
import { feedSelect } from '../screenobjects/components/FeedSelectors';
// import { groupSelect } from '../screenobjects/components/GroupsSelectors';

Given(/^I'm on the Groups screen$/, () => {
    feedSelect.SwitchedButtonGroup();
    feedSelect.WaitUpdateAppNo();
    feedSelect.UpdateAppNo();
});

When(/^I open the Groups$/, () => {});

Then(/^I click on button New Group$/, () => {});

When(/^ill input fields name: '(.+)', description: '(.+)'$/, () => {});

Then(/^I click on button create$/, () => {});

Then(/^I click on button create$/, () => {});

When(/^I click on button Administrator$/, () => {});

Then(/^Enter in my group$/, () => {});

Then(/^I click on button Edit$/, () => {});

Then(/^I'll alter the name to: '(.+)' and description to: '(.+)'$/, () => {});

When(/^I click on button save$/, () => {});

Then(/^return to the groups$/, () => {});

When(/^I click on button Administrator$/, () => {});

Then(/^Enter in my group$/, () => {});

When(/^I click on button create a publication$/, () => {});

When(/^I write a publication: '(.+)'$/, () => {});

When(/^I click on button send$/, () => {});

Then(/^return to the groups$/, () => {});
