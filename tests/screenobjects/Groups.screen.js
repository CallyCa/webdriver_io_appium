const SELECTORS = {
    InpuSearchGroups: '//android.view.View[@content-desc="Buscar grupos"]',
};

class SelectorsGroupsScreen {
    get SearchGroups() {
        return $(SELECTORS.InpuSearchGroups);
    }
}

export default new SelectorsGroupsScreen();
