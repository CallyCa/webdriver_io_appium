const SELECTORS = {
    CREATE_POST: '//android.widget.ImageView[@content-desc="Criar publicação"]',
    INPUT_TEXT: '//android.widget.EditText[@index="1"]',
    SEND_POST: '//android.widget.Button[@content-desc="Publicar"]',
    CREATE_COMMENT:
        '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText',
};

class SelectorsFeedScreen {
    // constructor() {
    //     super(SELECTORS.FORMS_SCREEN);
    // }

    // Feed Selectors

    get createPostsButton() {
        return $(SELECTORS.CREATE_POST);
    }

    get createPosts() {
        return $(SELECTORS.INPUT_TEXT);
    }

    get createPostsSend() {
        return $(SELECTORS.SEND_POST);
    }

    get inputComment() {
        return $(SELECTORS.CREATE_COMMENT);
    }

    /**
     * Return if the switch is active or not active for iOS / Android
     * For Android the switch is `ON|OFF`, for iOS '1|0'
     *
    //  * @return {boolean}
    //  */
    // isSwitchActive() {
    //     const active = driver.isAndroid ? 'ON' : '1';

    //     return this.switch.getText() === active;
    // }
}

export default new SelectorsFeedScreen();
