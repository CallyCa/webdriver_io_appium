const SELECTORS = {
    Register_FirstName: '//android.widget.EditText[index="1"]',
    Register_SirName: '//android.widget.EditText[index="2"]',
    Register_Email: '//android.widget.EditText[index="1"]',
    Register_NameCompany: '//android.widget.EditText[index="1"]',
};

class RegisterScreen {
    get RegisterNameUser() {
        return $(SELECTORS.Register_FirstName);
    }

    get RegisterSirNameUser() {
        return $(SELECTORS.Register_SirName);
    }

    get RegisterEmail() {
        return $(SELECTORS.Register_Email);
    }

    get RegisterCompanyName() {
        return $(SELECTORS.Register_NameCompany);
    }
}

export default new RegisterScreen();
