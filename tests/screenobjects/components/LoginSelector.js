class LoginSelector {
    returnButton() {
        $('//android.widget.Button').click();
    }

    alertBiometricMessageNO() {
        $('~NÃO').click();
    }

    alertBiometricMessageYes() {
        $('~SIM').click();
    }

    errorMessageLogin() {
        $('//android.view.View[@index="0"]').waitForDisplayed(20000);
    }

    errorMessageLoginClick() {
        $('~Ok').click();
    }

    waitForInputEmailShown() {
        $('//android.widget.EditText[@index="2"]').waitForDisplayed(20000);
    }

    waitForInputPasswdShown() {
        $('//android.widget.EditText[@index="3"]').waitForDisplayed(20000);
    }

    waitForBiometricShown() {
        $('Deseja habilitar o login com acesso biométrico?').waitForDisplayed(
            20000,
        );
    }
}

export const loginSelect = new LoginSelector();
