class RegisterSelector {
    returnButton() {
        $('//android.wid.Button').click();
    }

    WaitCreateUsersButton() {
        $(
            '//android.wid.Button[@content-desc="Novo por aqui? Cadastre-se!"]',
        ).waitForDisplayed(20000);
    }

    CreateUsersButton() {
        $(
            '//android.wid.Button[@content-desc="Novo por aqui? Cadastre-se!"]',
        ).click();
    }

    ClickButtonNext() {
        $('//android.wid.Button[@content-desc="vamos lá! "]').click();
    }

    CreateUserCommom() {
        $('//android.wid.Button[@content-desc="Sou um usuário"]').click();
    }

    CreateUserCompany() {
        $('//android.widget.Button[@content-desc="Sou uma empresa"]').click();
    }

    ClickButtonOK() {
        $('//android.wid.Button[@content-desc="pronto!"]').click();
    }
}

export const registerSelect = new RegisterSelector();
