class GroupsSelectors {
    ButtonClose() {
        $('//android.widget.Button').click();
    }

    SwitchedButtonGroup() {
        $('//android.view.View[@index="2"]').click();
    }

    ClickButtonYourGroups() {
        $(
            '//android.view.View[@content-desc="SEUS GRUPOS Guia 1 de 2"]',
        ).click();
    }

    ClickButtonMoreGroups() {
        $(
            '//android.view.View[@content-desc="MAIS GRUPOS Guia 2 de 2"]',
        ).click();
    }

    ButtonAdmin() {
        $('//android.widget.Button[@content-desc="Administrador"]').click();
    }

    EnterYourGroup() {
        $('//android.view.View[@content-desc="Squad Fortnite"]');
    }

    EditYourGroup() {
        $('//android.widget.Button[@content-desc="editar"]');
    }

    SelectPhoto() {
        $(
            '//android.widget.Button[@content-desc="Selecione imagem da galeria"]',
        );
    }

    SaveEdit() {
        $('//android.widget.Button[@content-desc="Salvar"]');
    }

    DeleteGroupButton() {
        $('//android.widget.Button[@content-desc="Excluir grupo"]');
    }

    DeleteGroupCancel() {
        $('//android.widget.Button[@content-desc="Não"]');
    }

    DeleteGroupConfirm() {
        $('//android.widget.Button[@content-desc="Sim"]');
    }

    EnterMoreGroups() {
        $('//android.view.View[@index="0"]');
    }

    EnterMoreGroupsButton() {
        $('//android.widget.Button[@content-desc="entrar"]');
    }

    ExitMoreGroupsButton() {
        $('//android.widget.Button[@content-desc="sair"]');
    }

    ExitGroupCancel() {
        $('//android.widget.Button[@content-desc="Não"]');
    }

    ExitGroupConfirm() {
        $('//android.widget.Button[@content-desc="Sim"]');
    }

    CreateNewGroup() {
        $('//android.widget.Button[@content-desc="NOVO GRUPO"]');
    }

    ButtonCreateGroup() {
        $('//android.widget.Button[@content-desc="Criar grupo"]');
    }
}

export const groupSelect = new GroupsSelectors();
