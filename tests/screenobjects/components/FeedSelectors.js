class FeedSelectors {
    returnButton() {
        $('//android.widget.Button').click();
    }

    UpdateAppNo() {
        $('~Não').click();
    }

    WaitUpdateAppNo() {
        $('~Não').waitForDisplayed(20000);
    }

    returnFavoriteButton() {
        $(
            '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.widget.Button',
        ).click();
    }

    openForms() {
        $('~Criar publicação').click();
    }

    sendPublication() {
        for (let i = 0; i < 5; i += 1) {
            $('//android.widget.Button[@content-desc="Publicar"]').click();
        }
    }

    waitForTabBarShown() {
        $('~Criar publicação').waitForDisplayed(20000);
    }

    buttonLike() {
        for (let i = 0; i < 5; i += 1) {
            $(
                '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.widget.Button[1]',
            ).click();
        }
    }

    waitButtonLike() {
        $(
            '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.widget.Button[1]',
        ).waitForDisplayed(20000);
    }

    VerifyButtonLike() {
        $(
            '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.widget.Button[1]',
        ).isDisplayed();
    }

    commentButton() {
        $('//android.widget.Button[@index="2"]').click();
    }

    sendComment() {
        $(
            '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText/android.widget.Button',
        ).click();
    }

    waitSendComment() {
        $(
            '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText/android.widget.Button',
        ).waitForDisplayed(20000);
    }

    favoritedButton() {
        $('//android.widget.Button[@index="4"]').click();
    }

    waitFavoritedButton() {
        $('//android.widget.Button[@index="4"]').waitForDisplayed(20000);
    }

    favoriteIntern() {
        $(
            '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.widget.Button',
        ).click();
    }

    menuOptions() {
        $('//android.widget.Button[@index="0"]').click();
    }

    menuOptionCopy() {
        $('~Copiar texto').click();
    }

    VerifyMenuOptionCopy() {
        $('~Copiar texto').isDisplayed();
    }

    VerifyMenuOptionReport() {
        $('~Denunciar conteúdo').isDisplayed();
    }

    menuOptionReport() {
        $('~Denunciar conteúdo').click();
    }

    menuOptionDelete() {
        $('~Excluir publicação').click();
    }

    VerifyMenuOptionDelete() {
        $('~menuOptionDelete').isDisplayed();
    }

    firstOptionReport() {
        $('~É spam').click();
    }

    modalMessageOk() {
        $('~Ok').click();
    }

    VerifyModalMessageOk() {
        $('~Ok').isDisplayed();
    }

    menuOptionDeleteCancel() {
        $('//android.widget.Button[@content-desc="Cancelar"]').click();
    }

    VerifyMenuOptionDeleteCancel() {
        $('//android.widget.Button[@content-desc="Cancelar"]').isDisplayed();
    }

    menuOptionDeleteConfirm() {
        $('//android.widget.Button[@content-desc="Confirmar"]').click();
    }

    VerifyMenuOptionDeleteConfirm() {
        $('//android.widget.Button[@content-desc="Confirmar"]').isDisplayed();
    }

    waitForTabBarShownMenu() {
        $('//android.widget.Button[@index="0"]').waitForDisplayed(20000);
    }
}

export const feedSelect = new FeedSelectors();
