class InformationsAccountSelectors {
    ButtonClose() {
        $('//android.widget.Button').click();
    }

    SwitchedButtonInformation() {
        $('//android.view.View[@index="4"]').click();
    }

    VerifySwitchedButtonInformation() {
        $(
            '//android.view.View[@content-desc="Mais Guia 5 de 5"]',
        ).waitForDisplayed(20000);
    }

    InviteButton() {
        $('~Convite').click();
    }

    WaitForInvite() {
        $(
            '//android.widget.Button[@content-desc="Link de compartilhamento"]',
        ).waitForDisplayed(20000);
    }

    InviteLinkButton() {
        $(
            '//android.widget.Button[@content-desc="Link de compartilhamento"]',
        ).click();
    }

    ClickInputInvite() {
        for (let i = 0; i < 2; i += 1) {
            $(
                '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[4]',
            ).click();
        }
    }

    VerifyInviteSelectAll() {
        $(
            '//android.widget.Button[@content-desc="Selecionar tudo"]',
        ).waitForDisplayed(20000);
    }

    InputInviteSelectAll() {
        $('//android.widget.Button[@content-desc="Selecionar tudo"]').click();
    }

    InputInviteCopyAll() {
        $('//android.widget.Button[@content-desc="Copiar"]').click();
    }

    VerifyInviteCopyAll() {
        $('//android.widget.Button[@content-desc="Copiar"]').isDisplayed();
    }

    ClickCopyLinkButton() {
        $(
            '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.ImageView',
        ).click();
    }

    VerifyClickCopyLinkButton() {
        $(
            '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.ImageView',
        ).isDisplayed();
    }

    InformationsButton() {
        $('~Informações da conta').click();
    }

    AlterPassword() {
        $('~Alterar senha').click();
    }

    AlterEmailButton() {
        $('~Alterar email').click();
    }

    Fingerprint() {
        $('~Acesso biométrico').click();
    }

    FingerprintSwitch() {
        $(
            '//android.widget.Switch[@content-desc="Reconhecimento Rápido"]',
        ).click();
    }

    FingerprintConfirmButton() {
        $('//android.widget.Button[@content-desc="Acessar!"]').click();
    }

    FingerprintCancelButton() {
        $('//android.view.View[@index="3"]').click();
    }

    BlockedUsers() {
        $(
            '//android.widget.ImageView[@content-desc="Usuários bloqueados"]',
        ).click();
    }

    UnblockUsers() {
        $('//android.widget.Button[@content-desc="DESBLOQUEAR"]').click();
    }

    UnblockUsersConfirm() {
        $('//android.widget.Button[@index="2"]').click();
    }

    UnblockUsersCancel() {
        $('//android.widget.Button[@index="3"]').click();
    }

    ConfigurationButton() {
        $('//android.widget.ImageView[@content-desc="Configurações"]').click();
    }

    ConfigurationsAcessibility() {
        $('//android.widget.ImageView[@content-desc="Acessibilidade"]').click();
    }

    AcessibilityViewEmoji() {
        $(
            '//android.widget.Switch[@content-desc="Visualização de emojis"]',
        ).click();
    }

    AcessibilityDescription() {
        $(
            '//android.widget.Switch[@content-desc="Descrição de emojis"]',
        ).click();
    }

    DarkMode() {
        $('//android.widget.Switch[@content-desc="Modo escuro"]').click();
    }

    ForDarkMode() {
        for (let i = 0; i < 3; i += 1) {
            $('//android.widget.Switch[@content-desc="Modo escuro"]').click();
        }
    }

    AboutUsButton() {
        $(
            '//android.widget.ImageView[@content-desc="Sobre a Tismoo.me"]',
        ).click();
    }

    PrivacityButton() {
        $('//android.widget.ImageView[@content-desc="Privacidade"]').click();
    }

    PrivacityTerms() {
        $(
            '//android.widget.Button[@content-desc="Termos de uso e aviso de privacidade"]',
        ).click();
    }

    PrivacityData() {
        $(
            '//android.widget.Button[@content-desc="Solicitar minhas informações atendendo à proteção de dados"]',
        ).click();
    }

    PrivacityDataConfirm() {
        $('//android.widget.Button[@index="3"]').click();
    }

    PrivacityDataCancel() {
        $('//android.widget.Button[@index="2"]').click();
    }

    HelpButton() {
        $('//android.widget.ImageView[@content-desc="Ajuda"]').click();
    }

    HelpSendButton() {
        $('//android.widget.Button[@content-desc="enviar!"]').click();
    }

    ExitButton() {
        $('//android.widget.ImageView[@content-desc="Sair"]').click();
    }

    ExitConfirmButton() {
        $('//android.widget.Button[@index="3"]').click();
    }

    ExitCancelButton() {
        $('//android.widget.Button[@index="2"]').click();
    }
}

export const informationSelect = new InformationsAccountSelectors();
