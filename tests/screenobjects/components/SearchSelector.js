class SearchSelector {
    returnButton() {
        $('//android.wid.Button').click();
    }

    WaitMenuSearchButton() {
        $(
            '//android.view.View[@content-desc="Busca Guia 2 de 5"]',
        ).waitForDisplayed(20000);
    }

    MenuSearchButton() {
        $('//android.view.View[@index="1"]').click();
    }

    ClickUserProfile() {
        $('//android.widget.ImageView[@index="0"]').click();
    }

    WaitClickUserProfile() {
        $('//android.widget.ImageView[@index="0"]').waitForDisplayed(10000);
    }

    ShowsClickUserProfile() {
        $('//android.widget.ImageView[@index="0"]').isDisplayed();
    }

    FollowUser() {
        $('//android.widget.Button[@content-desc="seguir"]').click();
    }

    WaitFollowUser() {
        $('//android.widget.Button[@content-desc="seguir"]').waitForDisplayed(
            20000,
        );
    }

    MenuOptionsProfileUser() {
        $(
            '//android.view.View[@content-desc="Diogo Lovato"]/android.widget.Button[1]',
        ).click();
    }

    MenuBlockUser() {
        $('//android.widget.ImageView[@content-desc="Bloquear Diogo"]').click();
    }

    BlockUser() {
        $('//android.widget.Button[@content-desc="Bloquear"]').click();
    }

    ConfirmBlockUser() {
        $('//android.widget.Button[@content-desc="Ok"]').click();
    }

    UnblockUser() {
        $('//android.widget.Button[@content-desc="Desbloquear"]').click();
    }

    ConfirmUnblockUser() {
        $('//android.widget.Button[@content-desc="Desbloquear"]').click();
    }
}

export const searchSelect = new SearchSelector();
