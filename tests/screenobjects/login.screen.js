const SELECTORS = {
    LOGIN_INPUT: '//android.widget.EditText[@index="2"]',
    PASSWORD_INPUT: '//android.widget.EditText[@index="3"]',
    RECOVERY_PASSWORD: '~Esqueci minha senha',
    BUTTON_LOGIN: '~Entrar',
    CREATE_USER: '~Novo por aqui? Cadastre-se!',
    UNMASK_PASSWORD: '//android.view.View[@index="0"]',

    // IOS: {
    //     ALERT: "-ios predicate string:type == 'XCUIElementTypeAlert'",
    // },
};

class LoginScreen {
    get signUpButton() {
        return $(SELECTORS.SIGN_UP_BUTTON);
    }

    get email() {
        return $(SELECTORS.LOGIN_INPUT);
    }

    get password() {
        return $(SELECTORS.PASSWORD_INPUT);
    }

    get recoveryPassword() {
        return $(SELECTORS.RECOVERY_PASSWORD);
    }

    get loginButton() {
        return $(SELECTORS.BUTTON_LOGIN);
    }

    get UnmaskPasswd() {
        return $(SELECTORS.UNMASK_PASSWORD);
    }

    login(email, password) {
        // this.UnmaskPasswd.click();
        this.email.setValue(email);
        this.password.setValue(password);
        if (driver.isKeyboardShown()) {
            driver.hideKeyboard();
        }
        this.UnmaskPasswd.click();
        this.loginButton.click();
    }

    loginMessagemOk() {
        this.alert.waitForIsShown();
        this.alert.pressButton('Cancel');
        this.alert.waitForIsShown(false);
    }
}

export default new LoginScreen();
