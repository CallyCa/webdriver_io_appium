const SELECTORS = {
    InputPassword: '//android.widget.EditText[@index="0"]',
    Email: '//android.widget.EditText[@index="2"]',
    ConfirmEmail: '//android.widget.EditText[@index="3"]',
    ConfirmEmailButton: '//android.widget.Button[@content-desc="Confirmar"]',
    NewPasswordInput: '//android.widget.EditText[@index="2"]',
    ConfirmPasswordInput: '//android.widget.EditText[@index="3"]',
    BiometricInput: '//android.widget.EditText[@index="1"]',
    UnmaskPassword: '//android.widget.Button[@index="0"]',
};

class SelectorsInformationScreen {
    /** GET EMAIL * */
    get ConfirmPasswdEmail() {
        return $(SELECTORS.InputPassword);
    }

    get Email() {
        return $(SELECTORS.Email);
    }

    get ConfirmEmail() {
        return $(SELECTORS.ConfirmEmail);
    }

    get ButtonConfirm() {
        return $(SELECTORS.ConfirmEmailButton);
    }

    /** GET PASSWORD * */

    get ConfirmPasswordInput() {
        return $(SELECTORS.InputPassword);
    }

    get NewPassword() {
        return $(SELECTORS.NewPasswordInput);
    }

    get ConfirmPassword() {
        return $(SELECTORS.ConfirmPasswordInput);
    }

    /** GET BIOMETRIC ACCESS * */

    get InputBiometric() {
        return $(SELECTORS.BiometricInput);
    }

    get Unmask() {
        return $(SELECTORS.UnmaskPassword);
    }

    ChangeEmail(password, email, confirmEmail) {
        this.ConfirmPasswdEmail.setValue(password);
        this.Email.setValue(email);
        this.ConfirmEmail.setValue(confirmEmail);
        if (driver.isKeyboardShown()) {
            driver.hideKeyboard();
        }
        this.ButtonConfirm.click();
    }

    ChangePassword(passwd, password, confirmPassword) {
        this.ConfirmPasswordInput.setValue(passwd);
        this.NewPassword.setValue(password);
        this.ConfirmPassword.setValue(confirmPassword);
        if (driver.isKeyboardShown()) {
            driver.hideKeyboard();
        }
        this.ButtonConfirm.click();
    }

    PutPasswordBiometric(password) {
        // this.Unmask.click()
        this.InputBiometric.setValue(password);
        this.Unmask.click();
        if (driver.isKeyboardShown()) {
            driver.hideKeyboard();
        }
    }
}

export default new SelectorsInformationScreen();
