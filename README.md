# Appium - WebDriver.IO

Este projeto segue com o propósito de integrar as seguintes ferramentas para testes que são: 
* Appium
* Cucumber
* WebDriverIO
* Mocha
* Chai
* Allure

Para qualquer adição de `.apk` ou `.app`, adicione no diretório `./apps`.

### OBS:

Antes de executar o projeto será necessário instalar o node 14.17.0.

O `@wdio/appium-service` está integrado neste boilerplate então você precisa apenas iniciar um servidor Appium em sua máquina com o comando:

```bash
$ yarn server
```

## Configuração
Este projeto usa uma configuração específica para iOS e Android, veja [configs](./config/).
Será notado que possuem dois arquivos de configuração para Android e iOS e um terceiro nomeado `wdio.shared.conf.js`, onde este arquivo de configuração contém todos os padrões, portanto, as configurações do iOS e do Android precisam apenas conter os recursos e etapas necessárias para a execução nos dois dispositivos.


Para qualquer alteração no funcionamento das configurações dos dispositivos em que serão executados, alterar as seguintes linhas abaixo:

```
config.capabilities = [
{
    'appium:noReset': true,
    'appium:fullReset': false,
    'appium:dontStopAppOnReset': true,
}
```

## Cucumber - Gherkin
> Neste diretório `./tests/features/` ficarão as features do Gherkin.


## Executar os testes:
```bash
// Para Android
$ yarn android.app

// Para iOS
$ yarn ios.app
```
## Relatórios/Reports
Rodando este comando gerará um allure report no diretório `./test-report/allure-report`:

```bash
$ yarn report:generate
```

Execute o comando abaixo para iniciar um servidor em sua máquina e abrir o relatório allure no navegador:

```bash
$ yarn report:open
```
## Eslint e Prettier
Para a checagem de código use o seguinte comando com lint:

```bash
$ yarn code:check
```

Para formatar o código com lint:

```bash
$ yarn code:format
```

## Documentações utilizadas

  * WebDriverIO: https://webdriver.io/docs/api/appium/
  * Mocha: https://mochajs.org/
  * Chai: https://www.chaijs.com/guide/styles/
  * Robot Framework: https://robotframework.org/#introduction
  * Robot Framework + Appium: http://serhatbolsu.github.io/robotframework-appiumlibrary/AppiumLibrary.html
  * Vídeo Aulas Robot Framework + Appium: encurtador.com.br/afXZ4
  * Gherkin: https://cucumber.io/docs/gherkin/reference/
  * BDD em Javascript: https://www.sitepoint.com/bdd-javascript-cucumber-gherkin/
  

